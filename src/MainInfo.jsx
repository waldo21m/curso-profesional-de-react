import React, {useContext} from "react";
import {FormContext} from "./Form";

export default () => {
  const context = useContext(FormContext);
  return (
    <div>
      <input
        type="email"
        value={context.email}
        onChange={(ev) => context.setEmail(ev.target.value)}/>
      <input
        type="password"
        value={context.password}
        onChange={(ev) => context.setPassword(ev.target.value)}/>
    </div>
  );
};
