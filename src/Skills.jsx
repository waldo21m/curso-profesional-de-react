import React, {useContext} from "react";
import {FormContext} from "./Form";

export default () => {
  const context = useContext(FormContext);

  const addToList = (value) => {
    console.log(':O');
    context.setLikes([value].concat(context.likes));
  };

  const removeFromList = (value) => context.setLikes(context.likes.filter(v => v !== value));

  return (
    <div>
      <label>
        <input
          type="checkbox"
          name="likes[]"
          onChange={(ev) => ev.target.checked ? addToList('Ruby') : removeFromList('Ruby')}
        />Ruby
      </label>
      <label>
        <input
          type="checkbox"
          name="likes[]"
          onChange={(ev) => ev.target.checked ? addToList('JavaScript') : removeFromList('JavaScript')}
        />JavaScript
      </label>
    </div>
  );
};
