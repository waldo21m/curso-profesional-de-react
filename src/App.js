import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0,
      updatedAt: null
    };

    this.updateCounter = this.updateCounter.bind(this);
  }

  componentDidMount() {
    console.log("Fuí creado!");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(prevState, this.state);
    if (prevState.contador !== this.state.contador) {
      this.setState({
        updatedAt: new Date()
      });
    }
    console.log("Fuí actualizado");
  }

  updateCounter() {
    this.setState({contador: this.state.contador + 1});
  };

  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={this.updateCounter}>Sumar</button>
      </>
    );
  }
}

export default App;
