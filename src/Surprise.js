import React, {useEffect} from 'react';

export default () => {
  useEffect(() => {
    import('./hello').then(mod => mod.default());
  }, []);

  return (
    <div>
      <p>Sorpresa <span role='img' aria-label='Surprise'>🎉</span></p>
    </div>
  );
};
