# Curso Profesional de React

## Lecciones:

* [Introducción](#module01)
    * [Lección #1 - Presentación al curso profesional de React](#lesson01)
    * [Lección #2 - ¿Qué es React?](#lesson02)
    * [Lección #3 - Hola mundo con React](#lesson03)
    * [Lección #4 - ¿Cómo usar React en una página?](#lesson04)
* [JSX - Templates](#module02)
    * [Lección #5 - Primeros pasos en JSX](#lesson05)
    * [Lección #6 - Elementos de React](#lesson06)
    * [Lección #7 - Mostrar componentes](#lesson07)
    * [Lección #8 - Expresiones JavaScript en JSX](#lesson08)
    * [Lección #9 - Condiciones](#lesson09)
    * [Lección #10 - Ciclos](#lesson10)
    * [Lección #11 - Listas](#lesson11)
* [Componentes](#module03)
    * [Lección #12 - Introducción a los componentes](#lesson12)
    * [Lección #13 - Definiendo los componentes](#lesson13)
    * [Lección #14 - El render](#lesson14)
    * [Lección #15 - Introducción a las props](#lesson15)
    * [Lección #16 - Props en práctica](#lesson16)
    * [Lección #17 - ¿Qué es el state?](#lesson17)
    * [Lección #18 - Ejercicios con el state](#lesson18)
    * [Lección #19 - Ciclo de vida de un componente](#lesson19)
    * [Lección #20 - Eventos](#lesson20)
    * [Lección #21 - Efectos secundarios](#lesson21)
* [Formularios](#module04)
    * [Lección #22 - Leyendo el valor de los controles](#lesson22)
    * [Lección #23 - Enviando formularios](#lesson23)
    * [Lección #24 - Manipulando el DOM directamente](#lesson24)
* [Code splitting](#module05)
    * [Lección #25 - ¿Qué es code splitting?](#lesson25)
    * [Lección #26 - Code splitting en práctica](#lesson26)
    * [Lección #27 - Imports dinámicos](#lesson27)
    * [Lección #28 - React lazy y Suspense](#lesson28)
* [React Context](#module06)
    * [Lección #29 - ¿Qué es React Context?](#lesson29)
    * [Lección #30 - React context en práctica](#lesson30)
    * [Lección #31 - Actualizando el contexto](#lesson31)
    * [Lección #32 - Otros ejemplos del uso de contexto](#lesson32)
* [¿Cómo funciona React?](#module07)
    * [Lección #33 - ¿Qué es el Virtual DOM?](#lesson33)
    * [Lección #34 - ¿Qué son los hooks?](#lesson34)
    * [Lección #35 - ¿Cómo funciona JSX?](#lesson35)
* [Componentes de clase](#module08)
    * [Lección #36 - ¿Qué son los componentes de clase?](#lesson36)
    * [Lección #37 - ¿Cómo definir un componente de clase?](#lesson37)
    * [Lección #38 - Trabajando con props](#lesson38)
    * [Lección #39 - Trabajando con el estado](#lesson39)
    * [Lección #40 - Preservando el contexto](#lesson40)
    * [Lección #41 - Métodos del ciclo de vida de un componente](#lesson41)
    * [Lección #42 - Ciclo de vida en un componente funcional](#lesson42)

## <a name="module01"></a> Introducción

### <a name="lesson01"></a> Lección #1 - Presentación al curso profesional de React
- - -
En este curso veremos temas prácticos y téoricos como:
* Definir un componente.
* Animaciones.
* Hooks en práctica.
* Eventos.
* Que es el Virtual DOM.
* Como funciona JSX.
* El render.
* Que es el state.

Se debe tener conocimientos sólidos en JavaScript.

### <a name="lesson02"></a> Lección #2 - ¿Qué es React?
- - -
Explicación de React.

### <a name="lesson03"></a> Lección #3 - Hola mundo con React
- - -
Creación de un hola mundo desde stackblitz.

### <a name="lesson04"></a> Lección #4 - ¿Cómo usar React en una página?
- - -
React no asume nada acerca de las tecnologías con las que funciona tu proyecto, más allá de que usarás React para las interfaces gráficas, no importa qué lenguaje de backend uses, o si no estás usando ninguno, si usas herramientas de automatización en el frontend o no.

Es por esto que existen distintas alternativas para comenzar a usar React en tu aplicación, desde las más simples que te permiten usar React para ciertas partes de tu página, hasta herramientas de automatización para proyectos hechos 100% con React.

La forma más simple y rápida de integrar React en tu sitio web, es agregar un par de tags de script para importar la librería, como se describe en la guía para Agregar React en 1 minuto, simplemente agrega estos scripts:

https://reactjs.org/docs/add-react-to-a-website.html#step-2-add-the-script-tags

Crea un componente y muéstralo en un contedor. Listo, tienes React en tu página web.

Alternativamente, puedes usar React como herramienta de automatización de tareas en el frontend, para integrar React, minificar tu código, puedes manualmente configurar tu flujo de trabajo de webpack para que integre React y sus herramientas. Sin embargo, el proceso puede ser laboreoso y propenso a errores.

Por esta razón, el equipo de React administra una herramienta de terminal de nombre “create-react-app” que hace justo lo que anuncia, crea una aplicación de React, preconfigurada y con todo lo necesario para comenzar a desarrollar.

Para usarla, necesitas tener Nodejs instalado en tu computadora y ejecutar el siguiente comando que inicia un nuevo proyecto:

```sh
npx create-react-app my-app
```

Asegúrate de cambiar la palabra my-app por el nombre de tu proyecto.

create-react-app es la forma más popular de iniciar un proyecto que usa React para todo el frontend.

Si deseas trabajar en un entorno en la nube que te permita acceder a tu proyecto desde cualquier dispositivo conectado a internet, existen algunas plataformas en línea con las que puedes crear un proyecto de React haciendo un solo click. Algunas opciones son: Codesandbox y Stackblitz, asegúrate de visitar los sitios web para seguir el flujo de creación de proyectos con React.

Alternativamente, si estás usando un framework MVC como Rails, Django, Laravel o similares, asegúrate de buscar los plugins correspondientes para cada framework, que te permitan integrar React.

Por último, puedes usar React a través de un framework basado en la librería, algunas opciones son Gatsby y Nextjs. Estos frameworks de React incluyen sus propias herramientas para la creación de nuevos proyectos.

Continuemos

## <a name="module02"></a> JSX - Templates

### <a name="lesson05"></a> Lección #5 - Primeros pasos en JSX
- - -
Como discutimos antes, React no incluye un lenguaje de plantillas como HTML, en cambio, las plantillas y los elementos que conforman una vista se escriben usando código de JavaScript.

React expone un método createElement que puedes usar para crear elementos de React a usar en una vista. El código que se muestra a continuación, crea un botón con el texto Enviar:

```sh
React.createElement(‘button’,{},’Enviar’);
```

Puedes usar el segundo argumento para enviar información hacia el elemento button que se crea:

```sh
const Btn = ()=>{
  return React.createElement("button",{
    onClick: ()=> alert("Hola")
  },"Enviar");
}
```

Puedes continuar usando createElement para representar tus vistas, sin embargo, encontrarás pronto que usar esta función hará que el código de tus vistas se vuelva extremadamente largo y verboso, además de difícil de leer y reutilizar.

Para solucionar esto, se introduce JSX. JSX extiende la sintaxis de JavaScript para representar vía etiquetas las declaraciones de React.createElement. Internamente, JSX usa la misma función para crear elementos, en el exterior, notarás que usar JSX hará tu código más expresivo y simple.

El código del botón que vimos antes, se vería así con JSX

```sh
const Btn () => <button onClick={() => alert(“Hola”)}> Enviar </button>
```

Puedes notar que no colocamos comillas alrededor de la declaración de la etiqueta button, esto es porque la sintaxis de JSX no es un string, de nuevo, es JavaScript.

Por último, recuerda que aunque, al igual que HTML, JSX usa los caracteres menor qué y mayor qué para representar elementos de React, HTML y JSX no son la misma tecnología, JSX nos permite embeber expresiones de JavaScript y pasar directamente nuestros datos a la declaración de nuestras vistas.

A lo largo del bloque irás aprendiendo la sintaxis de JSX y sus características principales, continuemos.

### <a name="lesson06"></a> Lección #6 - Elementos de React
- - -
Explicación de JSX, virtual DOM y explicación entre un elemento y un componente de React.

### <a name="lesson07"></a> Lección #7 - Mostrar componentes
- - -
Explicación del método render del react-dom, que solo lo usaremos habitualmente para montar nuestros componentes en el elemento raíz. Si en dado caso será llamado varias veces en el proyecto, lo que hará React es actualizar solo los elementos necesarios.

 Los self clousing tags son simplemente las etiquetas que se cierran a sí mismas y son muy usadas cuando llamamos componentes que no tendrán hijos con JSX. Por ejemplo:
 
 ```sh
const Saludo = () => {
  return <p>Hola</p>;
};

const App = () => {
  return <h1><Saludo/></h1>;
};
```

Las etiquetas que vemos en jsx en realidad son componentes que trae por defecto React.

### <a name="lesson08"></a> Lección #8 - Expresiones JavaScript en JSX
- - -
Explicación de la sintaxis de llaves en React.

### <a name="lesson09"></a> Lección #9 - Condiciones
- - -
Una de las formas que podemos condicionar en JSX es de la siguiente manera:

```sh
function SaludarEnIdiomas({idioma}) {
  if (idioma === "en") {
    return <span>Hello</span>;
  } else {
    return <span>Hola</span>;
  }
}

const Saludo = () => {
  const nombre = 'Eduardo';
  return <p><SaludarEnIdiomas idioma="en"/> {nombre}</p>;
};
```

Otra manera es cuando lo tenemos dentro de un return de la siguiente manera:

```sh
const Saludo = () => {
  const nombre = 'Eduardo';
  const idioma = 'es';
  return (
    <div>
      {
        idioma === 'es' && <p>Hola</p>
      }
      {
        idioma === 'en' && <p>Hello</p>
      }
    </div>
  );
};
```

Y la vieja confiable es usando el operador ternario:

```sh
const Saludo = () => {
  const nombre = 'Eduardo';
  const idioma = 'es';
  return (
    <div>
      {
        idioma === 'en' ? <p>Hello</p> : <p>Hola</p>
      }
    </div>
  );
};
```

Y también hay muchas formas válidas más.

### <a name="lesson10"></a> Lección #10 - Ciclos
- - -
Explicación de un ciclo for convencional:

```sh
const nombres = [
  'Uriel',
  'Eduardo',
  'Marines',
  'Cody'
];

function getNombres() {
  const elementosLista = [];
  for (var i = 0; i < nombres.length; i++) {
    elementosLista.push(<li>{nombres[i]}</li>);
  }
  return elementosLista;
}

const Nombres = () => {
  return <ul>{getNombres()}</ul>
};

const App = () => {
  return <div><Nombres/></div>;
};
```

Usando una sintaxis más moderna y legible sería de la siguiente manera:

```sh
const nombres = [
  'Uriel',
  'Eduardo',
  'Marines',
  'Cody'
];

const Nombres = () => {
  return <ul>{
    nombres.map(nombre => <li>{nombre}</li>)
  }</ul>
};

const App = () => {
  return <div><Nombres/></div>;
};
```

*Nota: El método map() crea un nuevo array con los resultados de la llamada a la función indicada aplicados a cada uno de sus elementos.*

### <a name="lesson11"></a> Lección #11 - Listas
- - -
En esta lección, vamos a solucionar el problema *Each child in a list should have a unique "key" prop.* que nos surgió de la lección anterior. Para ello, simplemente agregamos un props key de la siguiente manera:

```sh
const Nombres = () => {
  return <ul>{
    nombres.map((nombre, index) => <li key={index}>{nombre}</li>)
  }</ul>
};
```

Ese valor lo usa React en el caso de que si se modifique el arreglo, React sepa que se debe actualizar.

## <a name="module03"></a> Componentes

### <a name="lesson12"></a> Lección #12 - Introducción a los componentes
- - -
En una aplicación de React, organizamos nuestro código y la funcionalidad en componentes. Conceptualmente, puedes pensar en los componentes como piezas de lego, que juntas forman piezas más complejas, los componentes son las piezas de construcción de una aplicación basada en React.

De acuerdo a la documentación de React, un componente es como una función de JavaScript que recibe información y entrega una respuesta, esta respuesta son los elementos de React que deberán aparecer en la interfaz.

Una interfaz de React se compone de elementos de React que representan los elementos del DOM, como <div>, o <button>, además de elementos de React que vienen de un componente personalizado como <MiBoton>.

Estos componentes encapsulan toda la funcionalidad de un elemento de la interfaz, en un mismo lugar. Es en el componente donde se define qué se va a mostrar, qué datos recibimos, qué datos modificamos y cómo esos datos cambian la interfaz.

Esta arquitectura orientada a componentes nos ayuda a organizar y separar la complejidad de nuestra aplicación en elementos sencillos que cumplen tareas simples e independientes de los demás. Un formulario, por ejemplo, puede estar compuesto de distintos componentes como un botón, algunos controles de entrada de texto, selectores, un control de calendario, etc.

A través de esta arquitectura, aplicamos el principio de “divide y vencerás” sobre nuestra interfaz gráfica, en lugar de ver la interfaz como una gran base de código en constante actualización, vemos la interfaz como una colección de elementos que se ocupan de tareas sencillas y que, en conjunto, forman la interfaz completa.

Los componentes más complejos como un calendario, un slider, un formulario, etc. Se componen de pequeños componentes de tareas específicas, más adelante hablaremos de forma en como podemos separar nuestros componentes para hacerlos más simples y reutilizables.

En React, un componente se puede definir con una función, a éstos los llamamos componentes funcionales, y con una clase, a éstos los llamamos componentes de clase. Sin importar qué tipo de componente escribas, éstos reciben información del exterior y retornan un grupo de elementos de React que después se verán en la pantalla.

A lo largo de este tema continuaremos analizando las principales características de un componente, cómo reciben información, cómo hacer componentes dinámicos, cómo afectan la interfaz, etc.

Continuemos.

### <a name="lesson13"></a> Lección #13 - Definiendo los componentes
- - -
Los componentes los podemos definir de la siguiente manera:

```sh
function App() {
  return <h1>Hola mundo</h1>;
};
```

```sh
const App = () => {
  return <h1>Hola mundo</h1>;
};
```

```sh
const App = () => <h1>Hola mundo</h1>;
};
```

O también podemos definirlo como clases. Ejemplo:

```sh
class App extends Component {
  render() {
    return <h1>Hola mundo</h1>;
  }
}
```

Este último, desde la versión de React 16.8 está siendo desplazada y se recomienda el uso de componentes funcionales, sin embargo, aún tendrá mucho uso por los proyectos legacy en React por ejemplo.

### <a name="lesson14"></a> Lección #14 - El render
- - -
Consiste en representar de forma virtual las funciones o las clases de manera visual cuando hace necesario. React evaluará entre el DOM y el virtual DOM si existió cambios y en caso de lo hubo, ejecutará el método render solo en los sitios necesarios para no consumir muchos recursos.

### <a name="lesson15"></a> Lección #15 - Introducción a las props
- - -
Como menciona la documentación de React:

Conceptualmente, los componentes son como las funciones de JavaScript. Aceptan datos de entrada (llamados props) y retornan elementos de React que describen lo que debería aparecer en la pantalla.

Las props son la colección de datos que un componente recibe del contenedor padre, y que pueden usarse para definir los elementos de React que retornará el componente.

En términos prácticos, si un componente necesita recibir información para funcionar, la recibe vía props.

En términos técnicos, las props tienen ciertas características:
* Son inmutables, que es el adjetivo para lo que no se puede modificar o cambiar. Una prop no se modifica.
* Pueden tener un valor por defecto.
* Pueden marcarse como obligatorias, cuando un componenente no puede funcionar sin recibir una prop.

En JSX, las props se ven como los atributos de los elementos HTML:

```sh
<Btn value=”Enviar” />
```

Estas props pueden recibir un string o el resultado de una expresión de JavaScript, usando la sintaxis de llaves:

```sh
<Saludo value={` Hola ${nombre} `} />
```

En componentes funcionales, las props se reciben como argumentos de la función. 

```sh
const Btn = (props) => { return null; }
```

En clases se pueden leer vía la propiedad props del objeto:

```sh
class Btn extends React.Component{
  render() { console.log(this.props); return null; }
}
```

Continuemos aprendiendo sobre props en el siguiente tema, donde veremos valores por defecto.

### <a name="lesson16"></a> Lección #16 - Props en práctica
- - -
Ejemplo #1:

```sh
const nombres = [
  'Uriel',
  'Eduardo',
  'Marines',
  'Cody'
];

const Saludar = (props) => {
  return <p>Hola {props.nombre}</p>;
};

const App = () => {
  return <div><Saludar nombre='Uriel'/></div>;
};
```

Ejemplo #2 (Usando destructuring):

```sh
const Saludar = ({nombre}) => {
  return <p>Hola {nombre}</p>;
};

const App = () => {
  return <div><Saludar nombre='Uriel'/></div>;
};
```

Para setear props por defectos se hace de la siguiente manera:

```sh
const Saludar = ({nombre, idioma}) => {
  const saludo = idioma === 'es' ? 'Hola' : 'Hello';
  return <p>{saludo} {nombre}</p>;
};

Saludar.defaultProps = {
  idioma: 'en'
};

const App = () => {
  return <div><Saludar nombre='Uriel'/></div>;
};
```

La **forma recomendada** es usando parámetros por defecto de ES6 *(y asi no dependemos todo de React porque posiblemente en versiones posteriores será eliminada esta propiedad)*:

```sh
const Saludar = ({nombre, idioma = 'en'}) => {
  const saludo = idioma === 'es' ? 'Hola' : 'Hello';
  return <p>{saludo} {nombre}</p>;
};

const App = () => {
  return <div><Saludar nombre='Uriel'/></div>;
};
```

### <a name="lesson17"></a> Lección #17 - ¿Qué es el state?
- - -
Es la información que le pertenece al componente y puede ser modificada o actualizada para verse reflejada en la interfaz gráfica de nuestro componente.

### <a name="lesson18"></a> Lección #18 - Ejercicios con el state
- - -
Creación de un counter haciendo uso de hooks:

```sh
const Button = () => {
  const [counter, setCounter] = useState(0);
  return (
    <div>
      <p>Presionado: {counter}</p>
      <button onClick={() => setCounter(counter + 1)}>Click me!</button>
    </div>
  );
};

const App = () => {
  return <div><Button/></div>;
};
```

Si nos fijamos desde la consola podemos observar que solo se está modificando el counter y nada más.

### <a name="lesson19"></a> Lección #19 - Ciclo de vida de un componente
- - -
Cuando desarrollamos con React, a veces hace falta que dependiendo de una acción ya sea previa o posteriormente ejecutemos acciones. Esto se lo conoce como ciclos de vidas donde React se encarga de llamar estas acciones. En componentes de clases poseíamos métodos para lograr estos cometidos que son los siguientes:
* componentDidMount() *Cuando el componente se montaba*
* componentDidUpdate() *Cuando el componente se actualizaba*
* componentWillUnmount() *Cuando el componente se desmontaba*

*P.D. Existen más métodos pero estos son los más comunes.*

Con los hooks tenemos el método [useEffect](https://es.reactjs.org/docs/hooks-effect.html). Es un método que recibe como argumento una función que se ejecutará luego de cada render del componente. Ejemplo:

```sh
const Button = () => {
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    console.log("Me ejecuté");
  });
  return (
    <div>
      <p>Presionado: {counter}</p>
      <button onClick={() => setCounter(counter + 1)}>Click me!</button>
    </div>
  );
};

const App = () => {
  return <div><Button/></div>;
};
```

Si queremos ejecutarlo solamente al iniciar el componente, debemos pasar como segundo argumento un arreglo vacío a useEffect. Ejemplo:

```sh
const Button = () => {
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    console.log("Me ejecuté");
  }, []);
  return (
    <div>
      <p>Presionado: {counter}</p>
      <button onClick={() => setCounter(counter + 1)}>Click me!</button>
    </div>
  );
};

const App = () => {
  return <div><Button/></div>;
};
```

Y si queremos usarlo únicamente cuando se desmonte el componente, debemos retornar una función. Ejemplo:

```sh
const Button = () => {
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    console.log("Me ejecuté");
    return () => console.log("Adios");
  }, []);

  return (
    <div>
      <p>Presionado: {counter}</p>
      <button onClick={() => setCounter(counter + 1)}>Click me!</button>
    </div>
  );
};

const App = () => {
  const [showButton, setShowButton] = useState(true);
  return (
    <div>
      <button onClick={() => setShowButton(false)}>Eliminar botón</button>
      <div>
        {showButton && <Button/>}
      </div>
    </div>
  );
};
```

### <a name="lesson20"></a> Lección #20 - Eventos
- - -
En html convencional recibimos eventos en las funciones. En React recibimos algo que llamamos *SyntheticEvent* que es una instancia al evento y comparte las mismas propiedades y métodos practicamente.

```sh
const Saludo = () => {
  const [name, setName] = useState('');
  return (
    <div>
      <input type="text" onChange={(ev) => setName(ev.target.value)}/>
      <p>Hola {name}</p>
    </div>
  );
};

const App = () => {
  return <div><Saludo/></div>
};
```

### <a name="lesson21"></a> Lección #21 - Efectos secundarios
- - -
En programación, llamamos efectos secundarios a las modificaciones que alteran el estado de nuestro programa. Vamos a verlo en términos prácticos comparando dos funciones:

```sh
(x, y) => x + y; 

nombre = “”;
(value) => nombre = value;
```

Decimos que la primera función no produce efectos secundarios, porque la ejecución de la misma no altera nada fuera del alcance de esta función, podemos ejecutar esta función cuantas veces queramos, y nada cambiará.

Por otro lado, la segunda función cambia una variable fuera de la ejecución de la función, alterando el estado de la app. Este es un efecto secundario.

Cuando hablamos de React, si el componente ejecuta una operación que altera el estado global de la app, estaríamos produciendo un efecto secundario, en general, un componente debe hacer operaciones que alteren al componente mismo, y no más. Por supuesto que hay muchas excepciones, sin embargo, hay que tener en cuenta que el código que no produce efectos secundarios es menos impredecible y más fácil de debuggear.

Algunos ejemplos de efectos secundarios en un componente pueden ser, realizar peticiones a un servidor con AJAX, alterar el DOM manualmente, conectarse a una websocket, etc.

En un componente funcional, estas operaciones no se pueden ejecutar, ya que las funciones de un componente, no producen efectos secundarios.

Para poder ejecutar operaciones que produzcan efectos secundarios, podemos usar el hook useEffect.

Como aprendiste en el vídeo anterior, useEffect nos permite enviar una función que se ejecutará luego del render de una función, esta función puede producir efectos secundarios, de ahí el nombre del hook useEffect.

En términos prácticos, useEffect es el lugar perfecto para:
* Ejecutar código como parte del ciclo de vida del componente.
* Hacer peticiones AJAX.
* Actualizar el DOM directamente, por ejemplo para reproducir un vídeo.
* Logging de cambios.

useEffect recibe como segundo argumento después de la primer función un arreglo. En este arreglo puedes pasar variables que se usarán para determinar si el efecto debe ejecutarse o no. En la documentación de React podemos ver un muy buen ejemplo:

```sh
import React, {useState, useEffect} from 'react';

function Example() {
  const [count, setCount] = useState(0);
  useEffect(() => {
    document.title = `You clicked ${count} times`;
  },[count]); // Solo se ejecuta si count cambió entre un render y otro

  return (
    <div>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
```

Cuando pasamos un arreglo vacío, useEffect sólo se ejecutará una vez luego del primer render.

Por último, si una función se retorna del efecto, ésta se ejecutará luego del último render una vez que el componente desaparezca de la interfaz.

Continuemos.

## <a name="module04"></a> Formularios

### <a name="lesson22"></a> Lección #22 - Leyendo el valor de los controles
- - -
Para este módulo vamos a usar [JSONPlaceholder](https://jsonplaceholder.typicode.com/guide.html) para crear recursos. En esta lección, vamos a maquetar el formulario de la siguiente manera:

```sh
const Form = () => {
  let [title, setTitle] = useState('');
  let [body, setBody] = useState('');
  return (
    <form>
      <div>
        <label htmlFor="title">Título</label>
        <input type="text" id="title" onChange={(ev) => setTitle(ev.target.value)}/>
      </div>
      <div>
        <label htmlFor="body">Publicación</label>
        <textarea id="body" onChange={(ev) => setBody(ev.target.value)}/>
      </div>
      <input type="submit" value="Enviar"/>
    </form>
  );
};

const App = () => {
  return <div><Form/></div>;
};
```

En la siguiente lección veremos como enviar el formulario.

### <a name="lesson23"></a> Lección #23 - Enviando formularios
- - -
Debemos a toda costa romper el comportamiento habitual de un formulario. Algunos de los errores más comunes son:
* No usar las etiquetas form.
* No usar un input o un button type submit.

Para enviar un formulario usaremos el evento onSubmit. Ejemplo:

```sh
const Form = () => {
  let [title, setTitle] = useState('');
  let [body, setBody] = useState('');

  const sendForm = (ev) => {
    ev.preventDefault();
    fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({
        title,
        body,
        userId: 1
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then(response => response.json())
      .then(json => {
        setTitle('');
        setBody('');
        console.log(json)
      });
  };

  return (
    <form onSubmit={(ev) => sendForm(ev)}>
      <div>
        <label htmlFor="title">Título</label>
        <input type="text" id="title" value={title} onChange={(ev) => setTitle(ev.target.value)}/>
      </div>
      <div>
        <label htmlFor="body">Publicación</label>
        <textarea id="body" value={body} onChange={(ev) => setBody(ev.target.value)}/>
      </div>
      <input type="submit" value="Enviar"/>
    </form>
  );
};

const App = () => {
  return <div><Form/></div>;
};
```

### <a name="lesson24"></a> Lección #24 - Manipulando el DOM directamente
- - -
No se recomienda manipular el DOM de forma directa ya que perdemos los beneficios que nos otorga React. Sin embargo, podemos modificarlo haciendo uso del método [useRef](https://es.reactjs.org/docs/hooks-reference.html#useref). Ejemplo:

```sh
const Form = ({showed}) => {
  let [title, setTitle] = useState('');
  let [body, setBody] = useState('');

  const firstInput = useRef();

  useEffect(() => {
    //Actualizar el DOM
    if (showed) {
      console.log(firstInput);
      firstInput.current.focus();
    }
  }, [showed]);

  const sendForm = (ev) => {
    ev.preventDefault();
    fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({
        title,
        body,
        userId: 1
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then(response => response.json())
      .then(json => {
        setTitle('');
        setBody('');
        console.log(json)
      });
  };

  return (
    <form onSubmit={(ev) => sendForm(ev)}>
      <div>
        <label htmlFor="title">Título</label>
        <input
          type="text"
          id="title"
          value={title}
          onChange={(ev) => setTitle(ev.target.value)}
          ref={firstInput}
        />
      </div>
      <div>
        <label htmlFor="body">Publicación</label>
        <textarea id="body" value={body} onChange={(ev) => setBody(ev.target.value)}/>
      </div>
      <input type="submit" value="Enviar"/>
    </form>
  );
};

const Accordion = () => {
  const [show, setShow] = useState(false);
  return (
    <div>
      <button onClick={() => setShow(true)}>Mostrar formulario</button>
      {show && <Form showed={show}/>}
    </div>
  );
};

const App = () => {
  return <div><Accordion/></div>;
};
```

## <a name="module05"></a> Code splitting

### <a name="lesson25"></a> Lección #25 - ¿Qué es code splitting?
- - -
Consiste en dividir el código de React para llamarlos solamente cuando sean necesarios.

https://es.reactjs.org/docs/code-splitting.html

### <a name="lesson26"></a> Lección #26 - Code splitting en práctica
- - -
Para esta lección instalaremos una aplicación de React de la siguiente manera:

```sh
npx create-react-app code-splitting
cd code-splitting
npm start
```

El code-splitting consiste en dividir nuestro proyecto en módulos o distintos archivos con la finalidad de enviar nuestro JS por partes. De esta forma reducimos su peso y solo le damos uso a los componentes o archivos cuando sea estrictamente necesario.

### <a name="lesson27"></a> Lección #27 - Imports dinámicos
- - -
La mejor manera de introducir división de código en tu aplicación es a través de la sintaxis de import() dinámico.

Antes:

```sh
import { add } from './math';

console.log(add(16, 26));
```

Después:

```sh
import("./math").then(math => {
  console.log(math.add(16, 26));
});
```

Ejemplo:

```sh
const App = () => {
  const [showSurprise, setShowSurprise] = useState(false);

  useEffect(() => {
    import('./hello').then(mod => mod.default());
  }, []);

  return (
    <div>
      <button onClick={() => setShowSurprise(true)}>Mostrar sopresa</button>
      {
        showSurprise && <Surprise/>
      }
    </div>
  );
};
```

Donde hello es lo siguiente:

```sh
export default function hello() {
  console.log('Hola mundo');
}
```

Esto lo podemos ver en la consola de nuestro navegador en el apartado "red (o network)". Es un archivo 2.chunck.js

También podemos observar mejor este ejemplo si colocamos el useEffect es en el componente Surprise. Ejemplo:

App.js
```sh
const App = () => {
  const [showSurprise, setShowSurprise] = useState(false);
  return (
    <div>
      <button onClick={() => setShowSurprise(true)}>Mostrar sopresa</button>
      {
        showSurprise && <Surprise/>
      }
    </div>
  );
};
```

Surprise.js
```sh
export default () => {
  useEffect(() => {
    import('./hello').then(mod => mod.default());
  }, []);

  return (
    <div>
      <p>Sorpresa <span role='img' aria-label='Surprise'>🎉</span></p>
    </div>
  );
};
```

Desde el network podemos ver que si no le damos al botón "Mostrar sorpresa", no aparecerá el archivo 2.chunck.js

### <a name="lesson28"></a> Lección #28 - React lazy y Suspense
- - -
¿Cómo podemos hacer lo de la lección anterior pero con componentes? Se hace con React.lazy() de la siguiente manera:

Antes:
```sh
import Surprise from "./Surprise";
```

Después:
```sh
const Surprise = React.lazy(() => import('./Surprise'));
```

Además de esto, como dice la [documentación](https://es.reactjs.org/docs/code-splitting.html), el componente lazy debería entonces ser renderizado adentro de un componente Suspense, lo que nos permite mostrar algún contenido predeterminado (como un indicador de carga) mientras estamos esperando a que el componente lazy cargue. De esta manera, quedaría lo siguiente:

```sh
const Surprise = React.lazy(() => import('./Surprise'));

const App = () => {
  const [showSurprise, setShowSurprise] = useState(false);
  return (
    <div>
      <button onClick={() => setShowSurprise(true)}>Mostrar sopresa</button>
      {
        showSurprise && <Suspense fallback={<p>Cargando...</p>}><Surprise/></Suspense>
      }
    </div>
  );
};
```

En el navegador, podemos simular un *red lenta* para ver bien el comportamiento y eso lo hacemos escogiendo la opción *Good 3G* (o una más baja) en el apartado red o network. Podemos observar el fallback "Cargando..." y luego aparece la sorpresa.

**El Suspense debe estar lo más cerca del componente que se va a cargar de forma dinámica**, de lo contrario, si lo colocamos de esta manera:

```sh
const Surprise = React.lazy(() => import('./Surprise'));

const App = () => {
  const [showSurprise, setShowSurprise] = useState(false);
  return (
    <div>
      <Suspense fallback={<p>Cargando...</p>}>
        <button onClick={() => setShowSurprise(true)}>Mostrar sopresa</button>
        {
          showSurprise && <Surprise/>
        }
      </Suspense>
    </div>
  );
};
```

Podemos observar que todo el código que esté dentro del Suspense será reemplazado por el fallback hasta que cargue el componente.

## <a name="module06"></a> React Context

### <a name="lesson29"></a> Lección #29 - ¿Qué es React Context?
- - -
Context provee una forma de pasar datos a través del árbol de componentes sin tener que pasar props manualmente en cada nivel.

https://es.reactjs.org/docs/context.html

### <a name="lesson30"></a> Lección #30 - React context en práctica
- - -
Para esta parte instalaremos un proyecto de la siguiente manera:

```sh
npx create-react-app context-example
```

Para esta lección, crearemos un botón que va a recibir un tema especifíco del createContext(). Ejemplo:

App.js
```sh
const themes = {
  'dark': {
    backgroundColor: 'black',
    color: 'white'
  },
  'light': {
    backgroundColor: 'white',
    color: 'black'
  }
};

export const ThemeContext = React.createContext();

const App = () => {
  return (
    <div>
      <ThemeContext.Provider value={themes.light}>
        <Button/>
      </ThemeContext.Provider>
    </div>
  );
};
```

Button.js
```sh
import React, {useContext} from "react";
import {ThemeContext} from './App'

export default (props) => {
  const context = useContext(ThemeContext);
  return(
    <button
      style={{
        backgroundColor: context.backgroundColor,
        color: context.color
      }}
    >
      Click me!
    </button>
  );
};
```

Los consumidores deben estar dentro de los proveedores para que puedan usar todos los valores que indica dicho proveedor. Además, el contexto debe exportarse e importarse en los consumidores para que puedan hacer uso de ellos.

### <a name="lesson31"></a> Lección #31 - Actualizando el contexto
- - -
Para ello, hacemos uso de los estados de la siguiente manera:

```sh
const themes = {
  'dark': {
    backgroundColor: 'black',
    color: 'white'
  },
  'light': {
    backgroundColor: 'white',
    color: 'black'
  }
};

export const ThemeContext = React.createContext();

const App = () => {
  const [theme, setTheme] = useState(themes.dark);
  return (
    <div>
      <ThemeContext.Provider value={theme}>
        <Button/>
        <button onClick={() => setTheme(themes.light)}>Modo claro</button>
        <button onClick={() => setTheme(themes.dark)}>Modo oscuro</button>
      </ThemeContext.Provider>
    </div>
  );
};
```

Los valores que se pasan en el props value no son especiales, pueden ser valores que cambian gracias al estado y estos serán recibidos por los hijos del proveedor.

### <a name="lesson32"></a> Lección #32 - Otros ejemplos del uso de contexto
- - -
El ejemplo de los temas es uno de los más comunes que podemos encontrar para el contexto. Sin embargo, existen otros ejemplos como el formulario de múltiples pasos que haremos en la siguiente lección.

Sin duda, esta es la mejor forma de compartir valores entre componentes sin necesidad de los props y evitando el [prop-drilling](https://kentcdodds.com/blog/prop-drilling/)

App.js
```sh
import React from 'react';
import './App.css';
import Form from "./Form";

const App = () => {
  return (
    <div>
      <Form/>
    </div>
  );
};

export default App;
```

Form.jsx
```sh
import React, {useState} from "react";
import MainInfo from "./MainInfo";
import Skills from "./Skills";

export const FormContext = React.createContext();

export default () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [likes, setLikes] = useState("");

  return(
    <form>
      <FormContext.Provider value={{email, password, likes, setEmail, setPassword, setLikes}}>
        <MainInfo/>
        <Skills/>
      </FormContext.Provider>
      <div>
        <p>Email: {email}</p>
        <p>Contraseña: {password}</p>
        <p>Lenguajes: {likes}</p>
      </div>
    </form>
  );
};
```

MainInfo.jsx
```sh
import React, {useContext} from "react";
import {FormContext} from "./Form";

export default () => {
  const context = useContext(FormContext);
  return (
    <div>
      <input
        type="email"
        value={context.email}
        onChange={(ev) => context.setEmail(ev.target.value)}/>
      <input
        type="password"
        value={context.password}
        onChange={(ev) => context.setPassword(ev.target.value)}/>
    </div>
  );
};
```

Skills.jsx
```sh
import React, {useContext} from "react";
import {FormContext} from "./Form";

export default () => {
  const context = useContext(FormContext);

  const addToList = (value) => {
    console.log(':O');
    context.setLikes([value].concat(context.likes));
  };

  const removeFromList = (value) => context.setLikes(context.likes.filter(v => v !== value));

  return (
    <div>
      <label>
        <input
          type="checkbox"
          name="likes[]"
          onChange={(ev) => ev.target.checked ? addToList('Ruby') : removeFromList('Ruby')}
        />Ruby
      </label>
      <label>
        <input
          type="checkbox"
          name="likes[]"
          onChange={(ev) => ev.target.checked ? addToList('JavaScript') : removeFromList('JavaScript')}
        />JavaScript
      </label>
    </div>
  );
};
```

Ejemplos más completos seguro existen en internet pero el principio es saber usar bien el React Context.

## <a name="module07"></a> ¿Cómo funciona React?

### <a name="lesson33"></a> Lección #33 - ¿Qué es el Virtual DOM?
- - -
El DOM virtual (VDOM) es un concepto de programación donde una representación ideal o “virtual” de la IU se mantiene en memoria y en sincronía con el DOM “real”, mediante una biblioteca como ReactDOM. Este proceso se conoce como reconciliación.

Este enfoque hace posible la API declarativa de React: le dices a React en qué estado quieres que esté la IU, y se hará cargo de llevar el DOM a ese estado. Esto abstrae la manipulación de atributos, manejo de eventos y actualización manual del DOM que de otra manera tendrías que usar para construir tu aplicación.

Ya que “DOM virtual” es más un patrón que una tecnología específica, las personas a veces le dan significados diferentes. En el mundo de React, el término “DOM virtual” es normalmente asociado con elementos de React ya que son objetos representando la interfaz de usuario. Sin embargo, React también usa objetos internos llamados “fibers” para mantener información adicional acerca del árbol de componentes. Éstos pueden ser también considerados como parte de la implementación de “DOM virtual” de React.

https://es.reactjs.org/docs/faq-internals.html

### <a name="lesson34"></a> Lección #34 - ¿Qué son los hooks?
- - -
Hooks son una nueva característica en React 16.8. Estos te permiten usar el estado y otras características de React sin escribir una clase.

https://es.reactjs.org/docs/hooks-intro.html

### <a name="lesson35"></a> Lección #35 - ¿Cómo funciona JSX?
- - -
JSX es una extensión de JavaScript, que busca traer la sintaxis de los lenguajes tipo HTML/XML para definir el árbol de elementos de nuestros componentes de React.

La sintaxis de JSX por sí misma no puede ser leída o ejecutada por un navegador, para hacerlo, es necesario convertir la sintaxis de JSX en instrucciones válidas del lenguaje.

Para que este proceso pase se requiere de un pragma. En programación “pragma” hace referencia a una directiva para el compilador, en otras palabras, un pragma le da instrucciones al compilador para el proceso de compilación.

Usamos un pragma en JSX para indicarle al compilador, cómo debe traducir las instrucciones de JSX a JavaScript válido. El pragma es necesario ya que aunque JSX es muy popular en el uso de React, no es el único lugar donde podemos usar JSX.

En un proyecto de React preconfigurado como el que obtenemos de usar create-react-app, el pragma de JSX es React.createElement, esta es la función que usaremos para convertir la sintaxis de JSX en JavaScript válido:

JSX
```sh
<div> < Button /> < /div >
```

JavaScript
```sh
React.createElement("div",{}, React.createElement(Button) )
```

Si quieres usar JSX en otro contexto, con otro framework como Vue, sólo necesitas cambiar el pragma, para que JSX use la función necesaria dependiendo de la librería que estés usando.

Si regresamos al ejemplo anterior, podemos aprender algunas cosas importantes acerca de cómo usar JSX en React.

Como puedes ver, el resultado final hace uso de las variables React y Button, es por eso que, aunque no uses directamente React, debes importarlo en cualquier archivo que usa JSX:

```sh
import React from ‘react’;
<div>
   <Button />
</div>
```

Lo mismo pasa para cualquier componente que hayas creado, en este ejemplo Button.

JSX evaluará tus etiquetas y dependiendo de la primer letra de la declaración, sabrá si estás refiriendo a un componente tuyo o a un elemento nativo de la plataforma. En este caso, por ejemplo, podemos saber que div es un elemento de la plataforma, y Button es un componente que tú has creado.

## <a name="module08"></a> Componentes de clase

### <a name="lesson36"></a> Lección #36 - ¿Qué son los componentes de clase?
- - -
Un componente de clase es aquél que está definido con una clase de JavaScript. Esta clase debe tener dos particularidades:
* Primero, debe ser una clase de ES6 de JavaScript que herede de React.Component
* Debe poder implementar un método render() que retorne los elementos de React para la interfaz de dicho componente.

Históricamente, un componente de clase se diferencía de un componente funcional en dos principales cosas: La capacidad de administrar un estado propio del componente y la exposición de métodos que pueden ejecutar funcionalidad personalizada en distintas etapas del ciclo de vida de un componente.

Sin embargo, con la introducción de hooks en React 16, los componentes funcionales homologaron la funcionalidad de los componentes de clase en que ahora también pueden guardar un estado y ejecutar métodos del ciclo de vida de un componente.

En ese sentido, gran parte de la comunidad de desarrolladores que usan React, recomiendan usar componentes funcionales y hooks, en lugar de componentes de clase. Al mismo tiempo, el equipo que mantiene y desarrolla React, recomienda que no se re escriban componentes de clase en componentes funcionales a menos que sea necesario.

Esto quiere decir que en el futuro, React soportará ambas formas de declarar un método, con clases y con funciones. Esto significa que para un desarrollador nuevo de React es necesario conocer ambas formas de declarar componentes ya que es probable que en su trabajo día a día se encuentre con componentes de clase y componentes funcionales como parte de una misma aplicación.

Por último, cabe mencionar que algunos conceptos de React son más claros cuando se observan a través de la definición en un componente de clase, particularmente los métodos del ciclo de vida de un componente.

### <a name="lesson37"></a> Lección #37 - ¿Cómo definir un componente de clase?
- - -
Se define de la siguiente manera:

```sh
import React from 'react';

class App extends React.Component {
  render() {
    return <p>Hola mundo</p>;
  }
}

export default App;
```

### <a name="lesson38"></a> Lección #38 - Trabajando con props
- - -
Para esta lección, debemos instalar la dependencia:

```sh
npm install prop-types --save
```

Esta nos sirve para definir los tipos de las propiedades que recibe los componentes. Por ejemplo:

```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    console.log(this.props);
    return <p>Hola mundo</p>;
  }
}

export default App;
```

https://es.reactjs.org/docs/typechecking-with-proptypes.html

### <a name="lesson39"></a> Lección #39 - Trabajando con el estado
- - -
Ejemplo:

```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0
    };
  }
  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={() => this.setState({contador: this.state.contador + 1})}>Sumar</button>
      </>
    );
  }
}

export default App;
```

### <a name="lesson40"></a> Lección #40 - Preservando el contexto
- - -
Primera forma de preservar el contexto:

```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0
    };
  }

  updateCounter = () => {
    this.setState({contador: this.state.contador + 1});
  };

  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={this.updateCounter}>Sumar</button>
      </>
    );
  }
}

export default App;
```

La siguiente manera es la forma más habitual:

```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0
    };

    this.updateCounter = this.updateCounter.bind(this);
  }

  updateCounter() {
    this.setState({contador: this.state.contador + 1});
  };

  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={this.updateCounter}>Sumar</button>
      </>
    );
  }
}

export default App;
```

### <a name="lesson41"></a> Lección #41 - Métodos del ciclo de vida de un componente
- - -
Para un mejor entendimiento podemo observer [el siguiente diagrama](https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/) que explica el ciclo de vida de los componentes de clase de React.

Ejemplo de componentDidMount:
```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0
    };

    this.updateCounter = this.updateCounter.bind(this);
  }

  componentDidMount() {
    console.log("Fuí creado!");
  }

  updateCounter() {
    this.setState({contador: this.state.contador + 1});
  };

  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={this.updateCounter}>Sumar</button>
      </>
    );
  }
}

export default App;
```

Ejemplo de componentDidUpdate:
```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0
    };

    this.updateCounter = this.updateCounter.bind(this);
  }

  componentDidMount() {
    console.log("Fuí creado!");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("Fuí actualizado");
  }

  updateCounter() {
    this.setState({contador: this.state.contador + 1});
  };

  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={this.updateCounter}>Sumar</button>
      </>
    );
  }
}

export default App;
```

**Importante: No debemos colocar el setState dentro del componentDidUpdate porque se genera un bucle infinito.** Para ello podemos hacer cosas como este escenario:

```sh
import React from 'react';
import './App.css';
import PropTypes from 'prop-types';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contador: 0,
      updatedAt: null
    };

    this.updateCounter = this.updateCounter.bind(this);
  }

  componentDidMount() {
    console.log("Fuí creado!");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(prevState, this.state);
    if (prevState.contador !== this.state.contador) {
      this.setState({
        updatedAt: new Date()
      });
    }
    console.log("Fuí actualizado");
  }

  updateCounter() {
    this.setState({contador: this.state.contador + 1});
  };

  static defaultProps = {
    name: 'Cody'
  };

  static propTypes = {
    name: PropTypes.string
  };

  render() {
    return (
      <>
        <p>Contador: {this.state.contador}</p>
        <button onClick={this.updateCounter}>Sumar</button>
      </>
    );
  }
}

export default App;
```

### <a name="lesson42"></a> Lección #42 - Ciclo de vida en un componente funcional
- - -
Los métodos equivalentes del ciclo de vida de un componente funcional son los siguientes:
* useEffect con un array vacío equivale a componentDidMount.
* useEffect sin el array declarado o pasando solo los valores que se modifican en el array equivale a componentDidUpdate.
* useEffect retornando una función equivale a componentWillUnmount.